<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

$the_theme = wp_get_theme();
$container = get_theme_mod( 'understrap_container_type' );
?>

<?php	
wp_nav_menu( array( 
    'theme_location' => 'footer_menu', 
    'container_class' => 'fc-footer-menu p-5',
    'menu_class' => 'd-flex justify-content-around flex-wrap',
     ) ); 
?>

<?php get_sidebar( 'footerfull' ); ?>

<div class="wrapper" id="wrapper-footer">

	<div class="<?php echo esc_attr( $container ); ?>">

		<div class="row">

			<div class="col-md-12">

				<footer class="site-footer" id="colophon">

					<div class="site-info">

							<a href="<?php  echo esc_url( __( 'http://wordpress.org/','understrap' ) ); ?>"><?php printf( 
							/* translators:*/
							esc_html__( 'Proudly powered by %s', 'understrap' ),'WordPress' ); ?></a>
								<span class="sep"> | </span>
					
							<a href="<?php  echo esc_html(  $the_theme->get( 'ThemeURI' ) ); ?>">
								<?php printf( // WPCS: XSS ok.
								/* translators:*/
									esc_html__( 'Theme: %1$s', 'understrap' ), $the_theme->get( 'Name' )
								); ?>
							</a> 
								<span class="sep"> | </span>
				
							<a href="<?php  echo esc_html( $the_theme->get( 'AuthorURI' ) ); ?>">
								<?php printf( // WPCS: XSS ok.
								/* translators:*/
									esc_html__( 'Site Created By: %1$s', 'understrap' ), $the_theme->get( 'Author' ) );
								?>
							</a>
					</div><!-- .site-info -->

				</footer><!-- #colophon -->

			</div><!--col end -->

		</div><!-- row end -->

	</div><!-- container end -->

</div><!-- wrapper end -->

</div><!-- #page we need this extra closing tag here -->

<?php wp_footer(); ?>

</body>

</html>

