<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */

get_header();

$container   = get_theme_mod( 'understrap_container_type' );
?>

<?php if ( is_front_page() && is_home() ) : ?>
	<?php get_template_part( 'global-templates/hero' ); ?>
<?php endif; ?>

<div id="index-wrapper">
	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">
<!-- this is the header content for the front page. Should be made dynamic and updateable -->
		<div class="row">

			<!-- Do the left sidebar check and opens the primary div -->
			<?php get_template_part( 'global-templates/left-sidebar-check' ); ?>

			<main class="site-main" id="main">
				<!-- check if this is the front page. If it is the front page then see if there is a featured post for the front page. Display the front page featured post. If there is no front page featured post then just prceed as normal -->
			<?php if (is_front_page() || is_home()) : ?>
				<?php if( is_active_sidebar( 'fp-hero-sidebar' ) ) : ?>
					<div class="row fp-hero">
						<?php dynamic_sidebar( 'fp-hero-sidebar' ); ?>
					</div>
				<?php endif; ?>
				<?php if( is_active_sidebar( 'fp_contact_row' ) ) : ?>						
					<div class="row fp-contact-row p-4">
						<?php dynamic_sidebar( 'fp_contact_row' ); ?>
					</div>
				<?php endif; ?>
				<?php if( is_active_sidebar( 'fp_content_row' ) ) : ?>
					<div class="row fp-content-row">
							<?php dynamic_sidebar( 'fp_content_row' ); ?>
					</div>
				<?php endif; ?>
			<?php else:
					if ( have_posts() ) : 
					 /* Start the Loop */ 
						while ( have_posts() ) : the_post(); 

							/*
							 * Include the Post-Format-specific template for the content.
							 * If you want to override this in a child theme, then include a file
							 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
							 */
							get_template_part( 'loop-templates/content', get_post_format() );
						endwhile;
					else : 
						get_template_part( 'loop-templates/content', 'none' );
					endif;
				endif; 
			?> <!-- End the loop for the featured front page article -->

			</main><!-- #main -->

			<!-- The pagination component -->
			<?php understrap_pagination(); ?>
		<?php if ( !(is_front_page() || is_home()) ) : ?>
		<!-- Do the right sidebar check -->
			<?php get_template_part( 'global-templates/right-sidebar-check' ); ?>
		<?php endif; ?>
		

	</div><!-- .row -->

</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>
