<?php
function understrap_remove_scripts() {
    wp_dequeue_style( 'understrap-styles' );
    wp_deregister_style( 'understrap-styles' );

    wp_dequeue_script( 'understrap-scripts' );
    wp_deregister_script( 'understrap-scripts' );

    // Removes the parent themes stylesheet and scripts from inc/enqueue.php
}
add_action( 'wp_enqueue_scripts', 'understrap_remove_scripts', 20 );

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {

	// Get the theme data
	$the_theme = wp_get_theme();
    wp_enqueue_style( 'child-understrap-styles', get_stylesheet_directory_uri() . '/css/child-theme.min.css', array(), $the_theme->get( 'Version' ) );
    wp_enqueue_script( 'jquery');
	wp_enqueue_script( 'popper-scripts', get_template_directory_uri() . '/js/popper.min.js', array(), false);
    wp_enqueue_script( 'child-understrap-scripts', get_stylesheet_directory_uri() . '/js/child-theme.min.js', array(), $the_theme->get( 'Version' ), true );
    wp_enqueue_script( 'custom-scripts', get_stylesheet_directory_uri() . '/js/fc.js', array('jquery'), $the_theme->get( 'Version' ), true );
    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }
}

function add_child_theme_textdomain() {
    load_child_theme_textdomain( 'understrap-child', get_stylesheet_directory() . '/languages' );
}
add_action( 'after_setup_theme', 'add_child_theme_textdomain' );

add_filter( 'body_class','my_body_classes' );
function my_body_classes( $classes ) {

    $classes[] = 'scheme-gold';

    return $classes;

}

/*
*   This function registers multiple sidebars
*/

add_action( 'widgets_init', 'my_awesome_sidebar' );

function my_awesome_sidebar() {

  $my_sidebars = array(
    array(
        'name'          => 'Front Page Hero Row',
        'id'            => 'fp-hero-sidebar',
        'description'   => 'Front page hero.',
    ),
    array(
      'name'          => 'Front Page Contact Row',
      'id'            => 'fp_contact_row',
      'class'         => 'col-md',
      'description'   => 'Front Page Contact section',
      'before_widget' => '<div id="%1$s" class="widget col-md %2$s">',
    ),
    array(
      'name'          => 'Front Page Content Row',
      'id'            => 'fp_content_row',
      'class'         => 'col-md',
      'description'   => '2nd row of widgets below the front page hero',
      'before_widget' => '<div id="%1$s" class="widget col-md %2$s">',
    ),
    array(
      'name'          => 'FC Footer Widgets',
      'id'            => 'fc_footer_widgets',
      'description'   => 'Footer widgets',
      'class'         => 'd-flex justify-content-around',
      'before_widget' => '<div id="%1$s" class="widget col-sm-12 %2$s">',
    ),
  );

  $defaults = array(
    'name'          => 'Awesome Sidebar',
    'id'            => 'awesome-sidebar',
    'description'   => 'The Awesome Sidebar is shown on the left hand side of blog pages in this theme',
    'class'         => '',
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h2 class="widgettitle">',
    'after_title'   => '</h2>'
  );

  foreach( $my_sidebars as $sidebar ) {
    $args = wp_parse_args( $sidebar, $defaults );
    register_sidebar( $args );
  }

}

function fc_register_footer_menu() {
  register_nav_menu('footer_menu',__( 'Footer Menu' ));
}
add_action( 'init', 'fc_register_footer_menu' );


// set function for ticket shortcode. Should be refactored to be more modular. Styling should be removed from tags and put into stylesheet.
  function fc_tc_shortcode($atts) {
    extract( shortcode_atts( array(
        'ids' => '138,345,346',
    ), $atts, 'fc_tickets' ) );

    $ids = explode(',', $ids);

    ob_start(); //capture following output
    ?>
    <div class="container-fluid">
      <div class="row">
        <?php foreach ($ids as $tc_id): ?>
        <?php if(get_post_status ( $tc_id ) != 'private'): ?>
        <div class="col-lg-4 mb-3 mb-lg-0">
          <div class="ticket-container p-5 scheme-blue">
            <h3><?php echo get_the_title($tc_id) ?></h3>
            <p class="ticket-description"><?php echo get_post_field('post_content', $tc_id); ?></p>
            <div class="ticket-shortcode-container"><?php
              $ticket_sc = '[tc_ticket id="'. $tc_id . '" show_price="true" price_position="before" quantity="true"]';
              echo do_shortcode( $ticket_sc );
             ?></div>
          </div>
        </div>
      <?php endif; ?>
      <?php endforeach; ?>
      </div>
    </div>
    <?php
    return ob_get_clean();
}

add_shortcode('fc_tc_ticket', 'fc_tc_shortcode');

function fc_map_shortcode($atts) {
  extract( shortcode_atts( array(
      'zoom' => '12',
      'view' => '40.728,-73.97',
      'height' => '400px'
  ), $atts, 'fc_tickets' ) );

  ob_start(); //capture following output
  ?>
  <div id="mapdiv"></div>

  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css"
   integrity="sha512-Rksm5RenBEKSKFjgI3a41vrjkw4EVPlJ3+OiI65vTjIdo9brlAacEuKOiQ5OFh7cOI1bkDwLqdLw3Zg0cRJAAQ=="
   crossorigin=""/>

  <!-- Make sure you put this AFTER Leaflet's CSS -->
  <script src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js"
   integrity="sha512-/Nsx9X4HebavoBvEBuyp3I7od5tA0UzAxs+j83KgC8PU0kgB4XiK4Lfe4y4cgBtaRJQEIFCW+oC506aPT2L1zw=="
   crossorigin=""></script>

   <style>
   #mapdiv {
   height: <?php echo $height ?>;
   }
   </style>

   <script>
   var mymap = L.map('mapdiv').setView([<?php echo $view ?>], <?php echo $zoom ?>);
   L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 19,
    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
   }).addTo(mymap);

   mymap.scrollWheelZoom.disable();

   var NYU = L.marker([40.7278358, -73.9915369]).addTo(mymap);
   NYU.bindPopup('<a href="https://goo.gl/maps/QZ44o5reGKD2" title="get directions to New York University">New York University</a>');

   var greenburg = L.marker([40.730773, -73.999487]).addTo(mymap);
   greenburg.bindPopup('<a href="https://goo.gl/maps/H4jfhkQYSH32" title="get directions to Greenburg Lounge">Greenburg Lounge</a>');

   var brooklynBazaar = L.marker([40.7299690, -73.9547666]).addTo(mymap);
   brooklynBazaar.bindPopup('<a href="https://goo.gl/maps/YBrJtdWykrr" title="get directions to the Brooklyn Bazaar">Brooklyn Bazaar</a>');

   var starBar = L.marker([40.7050538, -73.9250917]).addTo(mymap);
   starBar.bindPopup('<a href="https://goo.gl/maps/Qhx7YK8DQcH2" title="get directions to the Star Bar">Star Bar</a>');

   var BlueQuarterBar = L.marker([40.7270323, -73.9885909]).addTo(mymap);
   BlueQuarterBar.bindPopup('<a href="https://goo.gl/maps/2kwxXe5TZn92" title="get directions to the Blue Quarter Bar">Blue Quarter Bar</a>');

   </script>
  <?php
  return ob_get_clean();
}

add_shortcode('fc_map', 'fc_map_shortcode');
