<?php
/**
 * Sidebar setup for footer full and for FC footer widget area.
 *
 * @package understrap
 */

$container   = get_theme_mod( 'understrap_container_type' );

?>

<?php if ( is_active_sidebar( 'fc_footer_widgets' ) ) : ?>

	<!-- ******************* The Footer Full-width Widget Area ******************* -->

	<div id="wrapper-fc-footer-widgets">

		<div class="<?php echo esc_attr( $container ); ?>" id="fc-footer-widgets-content" tabindex="-1">

			<div class="row">

				<?php dynamic_sidebar( 'fc_footer_widgets' ); ?>

			</div>
			
		</div>

	</div><!-- #wrapper-fc-footer-widgets -->

<?php endif; ?>

<?php if ( is_active_sidebar( 'footerfull' ) ) : ?>

	<!-- ******************* The Footer Full-width Widget Area ******************* -->

	<div class="wrapper" id="wrapper-footer-full">

		<div class="<?php echo esc_attr( $container ); ?>" id="footer-full-content" tabindex="-1">

			<div class="row">

				<?php dynamic_sidebar( 'footerfull' ); ?>

			</div>

		</div>

	</div><!-- #wrapper-footer-full -->

<?php endif; ?>
