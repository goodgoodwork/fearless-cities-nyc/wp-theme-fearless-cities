(function($){
	// Add classes to the Tickera table (their documentation sucks. if there is a hook that can do this I have no idea where or what it is.)
  if($('table.order-details').length){
    $('table.order-details').addClass('table table-striped table-bordered');
  }

// if there are no items in the cart, remove the cart widget
  if($('.tc_empty_cart').length){
  	$('.tc_cart_widget').remove();
  } else {
  	$('.tc_cart_widget h3').addClass('icon fa-shopping-cart');
  }
})(jQuery);